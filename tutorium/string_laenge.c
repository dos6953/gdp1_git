#include <stdio.h>
#include <string.h>

#define FALSE 0
#define TRUE 1

int stringLaenge(char string[]){
  int i = 0;
  while(string[i]!='\0'){
    i++;
  }
  return i;
}

int compareString(char string1[], char string2[]){
  for(int i=stringLaenge(string1);i>0;i--){
    if(string1[i]!=string2[i]){
      return FALSE;
    }
  }
  return TRUE;
}

void remove_by_index(char string[], int index){
  for(int i = index; i < stringLaenge(string) - 1; i++) {
    string[i] = string[i + 1];
  }
}

int remove_by_char(char string[], char symbol){
  int removed=0;
  for(int i=stringLaenge(string)-1;i>0;i--){
    if(string[i]==symbol){
      remove_by_index(string,i);
      removed++;
    }
  }
  return removed;
}


int main(void){
  char scan[256];
  while(compareString(scan,"Ende")!=TRUE){
    printf("String: ");
    fflush(stdout);
    scanf(" %99[^\n]",scan);
    printf("Länge: %d\n",stringLaenge(scan));
    int removed = remove_by_char(scan,' ');
    printf("String ohne Leerzeichen: %s\n", scan);
    printf("Anzahl Wörter: %d\n", removed+1);
    fflush(stdout);
  }
}
