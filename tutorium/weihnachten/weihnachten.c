#include <stdio.h>

#define MAX_PLAETZCHENDOSEN 50
#define MAX_ZUTATEN 50

#define START_AMOUNT_BUTTER 20
#define START_AMOUNT_ZUCKER 15
#define START_AMOUNT_EIER 13
#define START_AMOUNT_SCHOKOLADE 5
#define START_AMOUNT_ZIMT 5
#define START_AMOUNT_MARMELADE 5


#define START_AMOUNT_PLAETZCHEN_MARMELADE 0
#define START_AMOUNT_PLAETZCHEN_ZIMTSTERN 0
#define START_AMOUNT_PLAETZCHEN_SCHOKOKEKS 0

enum ResCodes{
  RES_OK,
  RES_FAILED,
};

enum Zutaten{
  ID_BUTTER,
  ID_ZUCKER,
  ID_EIER,
  ID_SCHOKOLADE,
  ID_ZIMT,
  ID_MARMELADE,
};

enum Plaetzchen{
  ID_PLAETZCHEN_MARMELADE,
  ID_PLAETZCHEN_ZIMTSTERN,
  ID_PLAETZCHEN_SCHOKOKEKS,
};

struct Plaetzchendose{
  int anzahl;
  char * sorte;
  int id;
};

struct Zutat {
  int anzahl;
  char * name;
  int id;
};

struct Baeckerei{
  struct Plaetzchendose * plaetzchendosen;
  struct Zutat  * zutaten;
  char * oberelfe;
};

struct Baeckerei initBaeckerei(void){
  // Zutaten
  struct Zutat butter;
  butter.anzahl=START_AMOUNT_BUTTER;
  butter.name = "Butter";
  butter.id = ID_BUTTER;

  struct Zutat zucker;
  zucker.anzahl=START_AMOUNT_ZUCKER;
  zucker.name = "Zucker";
  zucker.id = ID_ZUCKER;

  struct Zutat eier;
  eier.anzahl=START_AMOUNT_EIER;
  eier.name = "Eier";
  eier.id = ID_EIER;

  struct Zutat schokolade;
  schokolade.anzahl=START_AMOUNT_SCHOKOLADE;
  schokolade.name = "Schokolade";
  schokolade.id = ID_SCHOKOLADE;


  struct Zutat zimt;
  zimt.anzahl=START_AMOUNT_ZIMT;
  zimt.name = "Zimt";
  zimt.id = ID_ZIMT;

  struct Zutat marmelade;
  marmelade.anzahl=START_AMOUNT_MARMELADE;
  marmelade.name = "Marmelade";
  marmelade.id = ID_MARMELADE;

  struct Zutat zutaten[MAX_ZUTATEN] = {
    butter,
    zucker,
    eier,
    schokolade,
    zimt,
    marmelade,
  };

  // Plaetzchen
  struct Plaetzchendose plaetzchendose_marmelade = {
    START_AMOUNT_PLAETZCHEN_MARMELADE,
    "Marmeladenplaetzchen",
    ID_PLAETZCHEN_MARMELADE,
  };
  struct Plaetzchendose plaetzchendose_zimtstern = {
    START_AMOUNT_PLAETZCHEN_MARMELADE,
    "Zimtstern",
    ID_PLAETZCHEN_ZIMTSTERN,
  };
  struct Plaetzchendose plaetzchendose_schokokeks = {
    START_AMOUNT_PLAETZCHEN_MARMELADE,
    "Schokokeks",
    ID_PLAETZCHEN_SCHOKOKEKS,
  };

  struct Plaetzchendose plaetzchendosen[MAX_PLAETZCHENDOSEN]= {
    plaetzchendose_marmelade,
    plaetzchendose_zimtstern,
    plaetzchendose_schokokeks,
  };

  struct Baeckerei baeckerei;
  baeckerei.plaetzchendosen = plaetzchendosen;
  baeckerei.zutaten = zutaten;
  baeckerei.oberelfe = "elflord";

  return baeckerei;
};

void backe_Zimtstern(struct Baeckerei * baeckerei){
  baeckerei->zutaten[ID_ZIMT].anzahl--;
  baeckerei->plaetzchendosen[ID_PLAETZCHEN_ZIMTSTERN].anzahl++;
}

int main(void){

  struct Baeckerei baeckerei = initBaeckerei();
  printf("name Zimtstern : %s \n ",baeckerei.plaetzchendosen[ID_PLAETZCHEN_ZIMTSTERN].sorte);
  printf("Anzahl Zimtstern : %d \n ",baeckerei.zutaten[ID_ZIMT].anzahl);
  backe_Zimtstern(&baeckerei);
  printf("Anzahl Zimtstern : %d \n ",baeckerei.zutaten[ID_ZIMT].anzahl);
  

  return 0;

}
