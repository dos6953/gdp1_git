#include <stdio.h>
#include <stdlib.h>

#define ERROR 1
#define OK 0

int main(int argc, char * argv[]){
  if(argc != 2){
    printf("Flascher Syntax! Bitte nur Zahl und Skalierungsfaktor angeben\n");
    return ERROR;
  }
  int digit = atoi(argv[1]);
  int scale = atoi(argv[2]);
  printf("digit: %d\n", digit);
  printf("scale: %d\n", scale);
  return OK;
}
