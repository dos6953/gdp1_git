#include <stdio.h>
#include <stdlib.h>

void matrixPrint(int** m){
  int i;
  int i2;
  for(i=0;i<2;i++){
    for(i2=0;i2<=2;i2++){
      printf("m[%d][%d]:\t %d\n",i,i2,m[i][i2]);
    }
  }
}

int** matrixAdd(int** m1, int** m2, int zeilen, int spalten){
  int i;
  int i2;
  int** m3 = malloc(sizeof(int*)*spalten);
  for(i=0;i<spalten;i++){
    m3[i] = malloc(sizeof(int)*zeilen);
  }

  for(i=0;i<=spalten-2;i++){
    for(i2=0;i2<=zeilen;i2++){
      m3[i][i2] = m1[i][i2] + m2[i][i2];
    }
  }

  return m3;
}

int main(void){
  int i;
  int i2;

  int m1_size_rows = 2;
  int m1_size_cols = 3;
  int** m1 = malloc(sizeof(int*)*m1_size_rows);
  for(i=0;i<m1_size_rows;i++){
    m1[i] = malloc(sizeof(int)*m1_size_cols);
  }
  m1[0][0] = 20;
  m1[0][1] = 10;
  m1[0][2] = 5;
  m1[1][0] = 8;
  m1[1][1] = 9;
  m1[1][2] = 10;


  int m2_size_rows = 2;
  int m2_size_cols = 3;
  int** m2 = malloc(sizeof(int*)*m2_size_rows);
  for(i=0;i<m2_size_rows;i++){
    m2[i] = malloc(sizeof(int)*m2_size_cols);
  }
  m2[0][0] = 10;
  m2[0][1] = 12;
  m2[0][2] = 7;
  m2[1][0] = 10;
  m2[1][1] = 4;
  m2[1][2] = 8;


  printf("\n");
  matrixPrint(m1);

  printf("\n");
  matrixPrint(m2);
  printf("\n");

  int** m3 = matrixAdd(m1,m2,2,3);
  matrixPrint(m3);
  printf("\n");

  return 0;
}
