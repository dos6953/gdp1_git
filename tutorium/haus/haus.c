#include <stdio.h>

int main(int argc, char** argv){

  int baseHeight = 5;
  int baseWidth = 5;
  int ratioHeight = 3;
  int ratioWidth = 2;

  int row;
  int column;

  int clisize;
  if (argc!=1){
    clisize = atoi(argv[1]);
  } else {
    clisize = 1;
  }
  int height = baseHeight + (clisize*ratioHeight);
  int width = baseWidth + (clisize*ratioWidth);
  int height_middle = height/2;
  int width_middle = width/2;

  //control output
  printf("height: %d\n",height);
  printf("width: %d\n",width);


  //matrix for house
  char house [height][width];



  //insert whitespace everywhere
  for (row=0;row<height;row++){
    for (column=0;column<width;column++){
      house[row][column]=' ';
    }
  }


  int rooflevel=1;
  for (row=0;row<height;row++){
    for (column=0;column<width;column++){
      //first line of roof
      if(row==0){
        if(column==width_middle){
          house[row][column]='*';
        } else if (column==width_middle+rooflevel) {
          house[row][column]='\\';
        } else if (column==width_middle-rooflevel){
          house[row][column]='/';
        }
      }
      rooflevel++;
      if(row==1){
        if(column==width_middle+rooflevel){
          house[row][column]='\\';
        }else if (column==width_middle-rooflevel) {
          house[row][column]='/';
        }
      }
    }
  }

  //output
  for (row=0;row<height;row++){
    for (column=0;column<width;column++){
      printf("%c", house[row][column]);
    }
    printf("\n");
  }



}
