//63105168421

void collatz_rec(unsigned int n){
	printf("%d", n);
	if(n>1){
		if(n%2==0){collatz_rec(n/2);}
		else{
			collatz_rec(3*n+1);
		}
	}
}

#include <stdlib.c>

bool istVokal(char c){
	char vokale[] = "aeiou";
	int i;
	for(i=0;i<4;i++){
		if(vokale[i] == c){
			return true;
		}
	}
	return false;
}

int zaehleVokale(char* s){
	int i;
	int anzahl = 0;
	for(i=0;s[i] != '\0';i++){
		if(istVokal(s[i])){
			anzahl++;
		}
	}
	return anzahl;
}

char* nachB(char* orig){
	int laenge = (zahleVokale(orig)*2) + strlen(orig);
	char trans[laenge];
	int i;
	int itrans;
	
	for(i=0;i<strlen(orig);i++){
		if(!istVokal(orig[i])){
			trans[itrans] = orig[i];
			itrans++;
		} else {
			trans[itrans] = orig[i];
			trans[itrans+1] = 'b';
			trans[itrans+2] = orig[i];
			itrans += 3;
		}
	}
	return trans;
}