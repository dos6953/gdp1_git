// A simple variant of the game Snake
//
// Used for teaching in classes
//
// Author:
// Franz Regensburger
// Ingolstadt University of Applied Sciences
// (C) 2011
//
// The worm model

#include <curses.h>
#include "worm.h"
#include "board_model.h"
#include "worm_model.h"

// Data defining the worm

// The current heading of the worm
// These are offsets from the set {-1,0,+1}


// Initialize the worm
enum ResCodes initializeWorm(struct worm* aworm, int len_max, struct pos headpos, enum WormHeading dir, enum ColorPairs color) {

  // Initialize last usable index to len_max -1: theworm_maxindex
  aworm->maxindex = len_max -1;

  // Initialize headindex: theworm_headindex
  aworm->headindex = 0;

  // Set all elements to unused
  int i;
  for(i=0;i<=aworm->maxindex;i++){
    aworm->wormpos[i].x = UNUSED_POS_ELEM;
    aworm->wormpos[i].x = UNUSED_POS_ELEM;
  }

  // Initialize position of worms head
  aworm->wormpos[aworm->headindex] = headpos;

  // Initialize the heading of the worm
  setWormHeading(aworm, dir);

  // Initialze color of the worm
  aworm->wcolor = color;

  return RES_OK;
}

// Show the worms's elements on the display
// Simple version
void showWorm(struct worm* aworm) {
  // Due to our encoding we just need to show the head element
  // All other elements are already displayed
  placeItem(
            aworm->wormpos[aworm->headindex],
            SYMBOL_WORM_INNER_ELEMENT,aworm->wcolor);
}

bool isInUseByWorm(struct worm* aworm, struct pos new_headpos){
  int i = aworm->headindex;
  bool collision=false;
  do {
    if(new_headpos.x == aworm->wormpos[i].x && new_headpos.y == aworm->wormpos[i].y){
      collision=true;
      break;
    }
    // 3 reverse ringbuffer
    i = (i+aworm->maxindex) % (aworm->maxindex+1);
  } while (i != aworm->headindex && aworm->wormpos[i].x != UNUSED_POS_ELEM);

  return collision;
}

void cleanWormTail(struct worm* aworm){
  // Compute tailindex
  // FAIL 1 only did modulo theworm_maxindex without +1
  int tailindex = (aworm->headindex+1) % (aworm->maxindex+1);

  // Is the array element at tailindex already in use?
  if ( aworm->wormpos[tailindex].y!=UNUSED_POS_ELEM ) {
    // YES: place a SYMBOL_FREE_CELL at the tail's position
    placeItem(aworm->wormpos[tailindex],SYMBOL_FREE_CELL, COLP_FREE_CELL);
  }
}

void moveWorm(struct worm* aworm, enum GameStates* agame_state) {
  // Get current position of worms head element, but dont store them yet
  struct pos headpos;
  headpos = aworm->wormpos[aworm->headindex];

  headpos.x += aworm->dx;
  headpos.y += aworm->dy;

  // Check if we would leave the display if we move the worm's head according
  // to worm's last direction.
  // We are not allowed to leave the display's window.
  if (headpos.x < 0) {
    *agame_state = WORM_OUT_OF_BOUNDS;
  } else if (headpos.x > getLastCol() ) {
    *agame_state = WORM_OUT_OF_BOUNDS;
  } else if (headpos.y < 0) {
    *agame_state = WORM_OUT_OF_BOUNDS;
  } else if (headpos.y > getLastRow() ) {
    *agame_state = WORM_OUT_OF_BOUNDS;
  } else {
    // We will stay within bounds.
    // Check if worms head collides with itself at new position
    if(isInUseByWorm(aworm, headpos)){
      *agame_state = WORM_CROSSING;
    }
  }

  if(*agame_state == WORM_GAME_ONGOING){
    aworm->headindex = (aworm->headindex+1) % (aworm->maxindex+1);
    aworm->wormpos[aworm->headindex] = headpos;
  }
}

// Setters
void setWormHeading(struct worm* aworm, enum WormHeading dir) {
  switch(dir) {
  case WORM_UP :
    aworm->dx=0;
    aworm->dy=-1;
    break;
  case WORM_DOWN :
    aworm->dx=0;
    aworm->dy=1;
    break;
  case WORM_LEFT:
    aworm->dx=-1;
    aworm->dy=0;
    break;
  case WORM_RIGHT:
    aworm->dx=1;
    aworm->dy=0;
    break;
  }
}

struct pos getWormHeadPos(struct worm* aworm){
  // structs are passed by value
  // here a copy is returned
  return aworm->wormpos[aworm->headindex];
}
