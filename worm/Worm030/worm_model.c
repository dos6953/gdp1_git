// A simple variant of the game Snake
//
// Used for teaching in classes
//
// Author:
// Franz Regensburger
// Ingolstadt University of Applied Sciences
// (C) 2011
//
// The worm model

#include <curses.h>
#include "worm.h"
#include "board_model.h"
#include "worm_model.h"

// Data defining the worm
int theworm_maxindex;
int theworm_headindex;
int theworm_wormpos_y[WORM_LENGTH];  // Array of y-coordinates of the worm's head
int theworm_wormpos_x[WORM_LENGTH];  // Array of x-coordinates of the worm's head

// The current heading of the worm
// These are offsets from the set {-1,0,+1}
int theworm_dx;
int theworm_dy;

enum ColorPairs theworm_wcolor;

// Initialize the worm
enum ResCodes initializeWorm(int len_max, int headpos_y, int headpos_x, enum WormHeading dir, enum ColorPairs color) {

  // Initialize last usable index to len_max -1: theworm_maxindex
  theworm_maxindex = len_max -1;

  // Initialize headindex: theworm_headindex
  theworm_headindex = 0;

  // Set all elements to unused
  int i;
  for(i=0;i<=theworm_maxindex;i++){
    theworm_wormpos_y[i] = UNUSED_POS_ELEM;
    theworm_wormpos_x[i] = UNUSED_POS_ELEM;
  }

  // Initialize position of worms head
  theworm_wormpos_x[theworm_headindex] = headpos_y;
  theworm_wormpos_y[theworm_headindex] = headpos_x;

  // Initialize the heading of the worm
  setWormHeading(dir);

  // Initialze color of the worm
  theworm_wcolor = color;

  return RES_OK;
}

// Show the worms's elements on the display
// Simple version
void showWorm() {
  // Due to our encoding we just need to show the head element
  // All other elements are already displayed
  placeItem(
            theworm_wormpos_y[theworm_headindex],
            theworm_wormpos_x[theworm_headindex],
            SYMBOL_WORM_INNER_ELEMENT,theworm_wcolor);
}

bool isInUseByWorm(int new_headpos_x, int new_headpos_y){
  int i = theworm_headindex;
  bool collision=false;
  do {
    if(new_headpos_x == theworm_wormpos_x[i] && new_headpos_y == theworm_wormpos_y[i]){
      collision=true;
      break;
    }
    // FAIL 3 reverse ringbuffer
    i = (i+theworm_maxindex) % (theworm_maxindex+1);
  } while (i != theworm_headindex && theworm_wormpos_x[i] != UNUSED_POS_ELEM);

  return collision;
}

void cleanWormTail(){
  // Compute tailindex
  // FAIL 1 only did modulo theworm_maxindex without +1
  int tailindex = (theworm_headindex+1) % (theworm_maxindex+1);

  // Is the array element at tailindex already in use?
  if ( theworm_wormpos_y[tailindex]!=UNUSED_POS_ELEM ) {
    // YES: place a SYMBOL_FREE_CELL at the tail's position
    // FAIL 2 swapped x and y
    placeItem(theworm_wormpos_y[tailindex], theworm_wormpos_x[tailindex], SYMBOL_FREE_CELL, COLP_FREE_CELL);
  }
}

void moveWorm(enum GameStates* agame_state) {
  // Get current position of worms head element, but dont store them yet
  int headpos_x = theworm_wormpos_x[theworm_headindex];
  int headpos_y = theworm_wormpos_y[theworm_headindex];

  headpos_x += theworm_dx;
  headpos_y += theworm_dy;

  // Check if we would leave the display if we move the worm's head according
  // to worm's last direction.
  // We are not allowed to leave the display's window.
  if (headpos_x < 0) {
    *agame_state = WORM_OUT_OF_BOUNDS;
  } else if (headpos_x > getLastCol() ) {
    *agame_state = WORM_OUT_OF_BOUNDS;
  } else if (headpos_y < 0) {
    *agame_state = WORM_OUT_OF_BOUNDS;
  } else if (headpos_y > getLastRow() ) {
    *agame_state = WORM_OUT_OF_BOUNDS;
  } else {
    // We will stay within bounds.
    // Check if worms head collides with itself at new position
    if(isInUseByWorm(headpos_x, headpos_y)){
      *agame_state = WORM_CROSSING;
    }
  }

  if(*agame_state == WORM_GAME_ONGOING){
    theworm_headindex = (theworm_headindex+1) % (theworm_maxindex+1);
    theworm_wormpos_x[theworm_headindex] = headpos_x;
    theworm_wormpos_y[theworm_headindex] = headpos_y;
  }
}

// Setters
void setWormHeading(enum WormHeading dir) {
  switch(dir) {
  case WORM_UP :
    theworm_dx=0;
    theworm_dy=-1;
    break;
  case WORM_DOWN :
    theworm_dx=0;
    theworm_dy=1;
    break;
  case WORM_LEFT:
    theworm_dx=-1;
    theworm_dy=0;
    break;
  case WORM_RIGHT:
    theworm_dx=1;
    theworm_dy=0;
    break;
  }
}
