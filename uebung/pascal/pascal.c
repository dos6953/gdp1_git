#include <stdio.h>
#include <stdlib.h>

int pascal(int row, int col){
  /*
              1
            1   1
          1	  2 	 1
        1 	3 	3    1
      1 	4 	6   4    1
    1 	5 	10	 10	  5	 1

    1
    1   1
    1	  2 	1
    1 	3 	3    1
    1 	4 	6    4    1
    1 	5 	10	 10	  5	 1

    col==row for right 1s
    col==0 for left 1s
  */

  if(col==row || col==0){
    return 1;
  }
  return pascal(row-1,col-1)+pascal(row-1,col);
}

void drawPascal(int h){
  char* emtpy= "   ";
  int row;
  int i2;
  for(row=0;row<h;row++){
    for(i2=0;i2<(h-row);i2++){
      printf("%s",emtpy);
    }
    for(i2=0;i2<=row;i2++){
      printf("%6d",pascal(row,i2));
      /* printf("%s",emtpy); */
    }
    printf("\n");
  }

}

int main(int argc, char** argv){
  if(argc==0){
    return 1;
    printf("Please Enter Height!\n");
    printf("Usage: pascal [height]\n");
  }

  // height = width
  int height = atoi(argv[1]);
  drawPascal(height);

  return 0;
}
