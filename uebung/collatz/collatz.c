#include <stdio.h>

void collatz_rec(int n){
  printf("%d",n);
  if(n>1){
    if(n%2==0){
      collatz_rec(n/2);
    } else {
      collatz_rec(3*n+1);
    }
  }
}

int main(void){
  collatz_rec(6);

  return 0;
}
