#include <stdio.h>
#include <stdlib.h>

int** matrix_make(int numcols, int numrows){
  int** matrix = malloc(sizeof(int*)*numcols);
  int i;
  int j;
  for(i=0;i<numcols;i++){
    matrix[i] = malloc(sizeof(int)*numrows);
    for(j=0;j<numrows;j++){
      matrix[i][j] = 0;
    }
  }
  return matrix;
}

void matrix_replace_row_with_array(int ** matrix, int* array, int rowindex, int numcols){
  //array has to be size of numrows
  int j;
  for(j=0;j<numcols;j++){
    matrix[rowindex][j] = array[j];
  }
}

void matrix_print(int** matrix, int numcols, int numrows){
  int i;
  int j;
  for(i=0;i<numrows;i++){
    for(j=0;j<numcols;j++){
      printf("%5d",matrix[i][j]);
    }
    printf("\n");
  }
}

int** matrix_add(int** m1, int** m2, int numcols, int numrows){
  int i;
  int j;
  int** mnew = matrix_make(numcols,numrows);
  for(i=0;i<numrows;i++){
    for(j=0;j<numcols;j++){
      mnew[i][j] = m1[i][j] + m2[i][j];
    }
  }
  return mnew;
}

int** matrix_multiply(int** m1, int** m2, int m2_numcols, int m1_numrows){
  if(m2_numcols==m1_numrows){

    int** mnew = matrix_make(m2_numcols,m1_numrows);
    int i;
    int j;
    int i2;
    int j2;
    for(i=0;i<m1_numrows;i++){
      for(j=0;j<m1_numrows;j++){

        for(i2=0,i2=0;i2<m1_numrows;i2++){
          for(j2=0;j2<m1_numrows;j2++){
            /* printf("mult: %3d i: %2d  j: %2d\n",m1[i][j2] * m2[i2][j],i,j2); */
            mnew[i2][j2] += (m1[i][j2] * m2[i2][j]);
          }
        }

      }
    }
    return mnew;

  } else {
    printf("Die Multiplikation dieser Matrizen ist nicht definiert!\n");
    return 0;
  }
}

int main(void){
  int m1_numcols = 4;
  int m1_numrows = 4;
  int m1_col1[] = {1,2,3,4};
  int m1_col2[] = {1,2,3,4};
  int m1_col3[] = {1,2,3,4};
  int m1_col4[] = {1,2,3,4};
  int** m1 = matrix_make(m1_numcols,m1_numrows);
  matrix_replace_row_with_array(m1,m1_col1,0,m1_numcols);
  matrix_replace_row_with_array(m1,m1_col2,1,m1_numcols);
  matrix_replace_row_with_array(m1,m1_col3,2,m1_numcols);
  matrix_replace_row_with_array(m1,m1_col4,3,m1_numcols);


  int m2_numcols = 4;
  int m2_numrows = 4;
  int m2_col1[] = {4,3,2,1};
  int m2_col2[] = {4,3,2,1};
  int m2_col3[] = {4,3,2,1};
  int m2_col4[] = {4,3,2,1};
  int** m2 = matrix_make(m2_numcols,m2_numrows);
  matrix_replace_row_with_array(m2,m2_col1,0,m2_numcols);
  matrix_replace_row_with_array(m2,m2_col2,1,m2_numcols);
  matrix_replace_row_with_array(m2,m2_col3,2,m2_numcols);
  matrix_replace_row_with_array(m2,m2_col4,3,m2_numcols);

  matrix_print(m1,m1_numcols,m1_numrows);
  printf("\n");
  matrix_print(m2,m2_numcols,m2_numrows);
  printf("\n");

  int** addedmatrix = matrix_add(m1,m2,m1_numcols,m1_numrows);
  matrix_print(addedmatrix,m1_numcols,m1_numrows);
  printf("\n");

  int** mulmatrix = matrix_multiply(m1,m2,m2_numcols,m1_numrows);
  matrix_print(mulmatrix,m2_numcols,m1_numrows);
  printf("\n");
  return 0;
}
