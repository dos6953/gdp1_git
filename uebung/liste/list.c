#include <stdio.h>
#include <stdlib.h>

typedef struct node {
  int data;
  struct node* next;
}node_t;


node_t* create_node(int data){
  node_t* node = malloc(sizeof(node_t*));
  node->data = data;
  node->next = NULL;
  return node;
}

node_t* insert_front(node_t* anchor, int data){
  node_t* newnode = create_node(data);
  newnode->next = anchor;
  return newnode;
}

node_t* insert_end(node_t* nodep, int data){
  if(nodep == NULL){
    return create_node(data);
  }
  nodep->next = insert_end(nodep->next, data);
  return nodep;
}

void print_list(node_t* nodep){
  node_t* currentnode = nodep;
  while(currentnode->next != NULL){
    printf("%d\n",currentnode->data);
    currentnode = currentnode->next;
  }
  // print last item
  printf("%d\n",currentnode->data);
}

node_t* insert_at_rec(node_t* nodep, int data, int index){
  node_t* currentnode = nodep;
  if(index == 0){
    currentnode = insert_front(currentnode,data);
    return currentnode;
  }
  currentnode = insert_at_rec(nodep->next, data, index-1);
  return currentnode;
}

node_t* insert_at(node_t* nodep, int data, int index){
  node_t* lastnode = nodep;
  node_t* currentnode = nodep->next;
  node_t* nextnode = nodep->next->next;
  int i;
  if(index==0){
    nodep = insert_front(nodep,data);
    return nodep;
  } else {
    for(i=1;i<index;i++){
      lastnode = currentnode;
      currentnode = nextnode;
      nextnode = nextnode->next;
    }
    node_t* newnode = create_node(data);
    lastnode->next = newnode;
    newnode->next = currentnode;
    return nodep;
  }
}

node_t* delete_at(node_t* nodep, int index){
  node_t* lastnode = nodep;
  node_t* currentnode = nodep->next;
  node_t* nextnode = nodep->next->next;
  int i;
  if(index==0){
    return currentnode;
  }else{
    for(i=1;i<index;i++){
      lastnode = currentnode;
      currentnode = nextnode;
      nextnode = nextnode->next;
    }
    lastnode->next = nextnode;
    return nodep;
  }
}

int access_data_at(node_t* anchor, int index){
  if(index==0){
    return anchor->data;
  } else {
    node_t* currentnode = anchor;
    int i;
    for(i=0;i<index;i++){
      currentnode = currentnode->next;
    }
    return currentnode->data;
  }
}

int sizeof_list(node_t* anchor){
  node_t* currentnode = anchor;
  int length = 0;
  while(currentnode->next != NULL){
    currentnode = currentnode->next;
    length++;
  }
  return length;
}


int main(void){

  node_t* mylist = NULL;
  mylist = insert_front(mylist,8);
  mylist = insert_front(mylist,7);
  mylist = insert_front(mylist,5);
  mylist = insert_front(mylist,4);
  mylist = insert_end(mylist,9);
  mylist = insert_at(mylist,6,0);
  /* mylist = delete_at(mylist,2); */

  print_list(mylist);
  int index = 0;
  printf("data at index %d :  %d\n",index,access_data_at(mylist,index));
  printf("sizeof list : %d\n",sizeof_list(mylist));

  return 0;
}
