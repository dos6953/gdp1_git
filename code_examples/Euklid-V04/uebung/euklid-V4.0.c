#include <stdio.h>
#define OK 0
#define ERROR 1


int ggt(int a, int b){
  //fix für negative werte
  if(a<0){a=-a;}
  if(b<0){b=-b;}

  // Eingegebene Werte i1, i2 in a, b umspeichern: Wieso?
  while (a != b) {
    if (a < b) {
      b = b - a;
    } else {
      a = a - b;
    }
  }
  return a;
}

int readInteger(char prompt[], int* n){
  int rescode;
  int versuche=2;
  int erfolg=0;

  while(versuche>0 && !erfolg){
    printf("%s",prompt);
    fflush(stdout);

    if(scanf("%d", n) != 1) {
      printf("Fehler bei der Eingabe der Zahl \n");
      versuche--;
      printf("Verbleibende Versuche: %d\n",versuche);
    } else {
      erfolg=1;
    }
    while(getchar() != '\n');

  }
  if(erfolg){
    rescode = OK;
  } else {
    rescode = ERROR;
  }
  return rescode;
}

int main (void) {
  int i1;
  int i2;

  /* Lies zwei ganze Zahlen in i1 und i2 ein */
  if (readInteger("i1 = ",&i1)!=OK){
    return ERROR;
  }

  if (readInteger("i2 = ",&i2)!=OK){
    return ERROR;
  }

  printf ("Der groesste gemeinsame Teiler von %d und %d ist: %d\n", i1, i2, ggt(i1,i2));
  return OK;
}
